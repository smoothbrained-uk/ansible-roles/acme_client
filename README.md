# Ansible for acme.sh

Ansible role for deployment of auto-renewing acme.sh on Debian and FreeBSD.

```yaml
acme_client:
  accounts:
    - domains:
        - "example.co.uk"
        - "*.example.co.uk"
      zerossl:
        email: "greg@example.co.uk"
      dnsapi:
        provider: mythic_beasts
        mythic_beasts_api_key: "{{ mythic_beasts_api_key }}"
        mythic_beasts_api_secret: "{{ mythic_beasts_api_secret }}"
      reload_commands:
        - "systemctl reload nginx"
      cron:
        mailto: "greg@example.co.uk"
```

## DNS API providers
### Mythic Beasts
You will need to generate an API key on the mythic beasts control panel with access to create TXT records at least for for the `_acme-challenge` subdomain of the domain you are issuing certificates for.

See [Mythic beasts support](https://www.mythic-beasts.com/support/domains/letsencrypt_dns_01) for more info.

The `dnsapi` vars for mythic beasts are:
- `provider`: The string `mythic_beasts`
- `mythic_beasts_api_key`: The api key generated on the mythic beasts control panel
- `mythic_beasts_api_secret`: The api secret generated on the mythic beasts control panel

Example:
```yaml
acme_client:
  accounts:
    - domains:
        - "example.co.uk"
        - "*.example.co.uk"
      zerossl:
        email: "greg@example.co.uk"
      dnsapi:
        provider: mythic_beasts
        mythic_beasts_api_key: "{{ mythic_beasts_api_key }}"
        mythic_beasts_api_secret: "{{ mythic_beasts_api_secret }}"
      reload_commands:
        - "systemctl reload nginx"
      cron:
        mailto: "greg@example.co.uk"
```

See: https://github.com/acmesh-official/acme.sh/wiki/dnsapi2#dns_mythic_beasts

### Google Domains
You will need to generated an API token in your google domains account to use the DNS api. See [Google - Use HTTPS on your domain](https://support.google.com/domains/answer/7630973?sjid=6849578087161710951-EU) for more info.

The `dnsapi` vars for mythic beasts are:
- `provider`: The string `googledomains`
- `googledomains_access_token`: The access token created

Example:
```yaml
acme_client:
  accounts:
    - domains:
        - "example.co.uk"
        - "*.example.co.uk"
      zerossl:
        email: "greg@example.co.uk"
      dnsapi:
        provider: googledomains
        googledomains_access_token: "{{ googledomains_token }}"
      reload_commands:
        - "systemctl reload nginx"
      cron:
        mailto: "greg@example.co.uk"
```

See: https://github.com/acmesh-official/acme.sh/wiki/dnsapi2#dns_googledomains

### Go Daddy

Generate an API key and secret pair from the developer dashboard:
<https://developer.godaddy.com/keys>

The `dnsapi` vars for Go Daddy are:

- `provider`: The string `gd`
- `gd_api_key`: The api key generated on the Go Daddy developer dashboard
- `gd_api_secret`: The api secret generated on the Go Daddy developer dashboard

Example:

```yaml
acme_client:
  accounts:
    - domains:
        - "example.co.uk"
        - "*.example.co.uk"
      zerossl:
        email: "greg@example.co.uk"
      dnsapi:
        provider: gd
        gd_api_key: "{{ gd_api_key }}"
        gd_api_secret: "{{ gd_api_secret }}"
      reload_commands:
        - "systemctl reload nginx"
      cron:
        mailto: "greg@example.co.uk"
```

See: https://github.com/acmesh-official/acme.sh/wiki/dnsapi#4-use-godaddycom-domain-api-to-automatically-issue-cert

### Adding support
Adding additional support can be done by adding additonal environment variables to the issue command in `tasks/account.yaml` and listing further API providers in `dnsapi_providers`. See [Defaults](#Defaults) for more info on `dnsapi_providers`.

## ZeroSSL
`acme_client` uses `acme.sh` to create certificates. As of v3 `acme.sh` uses [ZeroSSL](https://zerossl.com/) as the default CA (rather than LetsEncrypt). ZeroSSL does not impose rate-limits on users, but does require that you make an account with them (by just registering an email address).

Thus to use `acme_client` you will need to provide the email address you have registered with ZeroSSL. Sign up for ZeroSSL [here](https://app.zerossl.com/signup).

## Vars
`acme_client` is the dictonary which contains configuration for `acme.sh` and `cron`. It descends into the following subdictionaries:
- `accounts`: List - Each account listed in `accounts` is created a 'home' directory for `acme.sh` to use. Information about the ZeroSSL account and generated certifiates are stored in this directory. `accounts` is a list of dictionaries consisting of the following:
  - `domains`: A list of domains to generate a certificate for. The first domain listed is used as the name of the certificate for installation.
  - `ca`: The CA to use for requesting the cert. Defaults to `zerossl`. See [acme.sh - Supported CAs](https://github.com/acmesh-official/acme.sh#supported-ca).
  - `zerossl`:
    - `email`: The email to use to register this instance of acme.sh with ZeroSSL. Only required if `ca` is 'zerossl' (the default).
  - `dnsapi`: Contains info about the DNS api. The provider given in `provider` is used as a prefix for variables that define the API key user and secret (e.g. if `provider` == `'mythic_beasts'` then api key is defined with `mythic_beasts_api_secret`)
    - `provider`: The DNS api provider to use. Please see [DNS API providers](#DNS-API-providers) and [acme.sh wiki](https://github.com/acmesh-official/acme.sh/wiki/dnsapi) for supported providers.
      - `<provider>_api_key`: The DNS API key/user
      - `<provider>_api_secret`: The DNS API secret/password
  - `reload_commands`: A list of commands to run whenever a certificate is successfully renewed.
  - `cron`:
    - `exp`: A cron expression that determines when certificates are renewed. By default cert renewal is attempted at midnight every day.
    - `mailto`: (Optional) An email to give to `MAILTO` in cron to receive email notifications.
  - `certname`: (Optional) The name of this certificate/key pair. E.g. for `certname: "mycert"` will create 'mycert.cert', 'mycert\_fullchain.cert' and 'mycert.key' in `acmesh_install_path`. If undefined, the first domain listed in `domains` is used for the filename.
  - `keyreaders`: (Optional) A list of users that are able to read the private key for this certificate. This is mplemented using [xattrs](https://en.wikipedia.org/wiki/Extended_file_attributes) - the role will attempt to detect if the filesystem is using POSIX or NFSv4 ACLs and set them accordingly. If it detects that xattrs are not supported on the filesystem then this config is ignored. 

## Defaults
- `acmesh_user`: The user than runs the cron job to renew domain certificates. Defaults to `'root'`.
- `acmesh_path`: The path where `acme.sh` is installed (or is to be installed). Defaults to `/usr/local/sbin/acme.sh` on Freebsd and `/sbin/acme.sh` on Linux.
- `acmesh_examples_path`: The path where acme.sh's example dnsapi hooks are installed (or are to be installed). Defaults to `/usr/local/examples/acme.sh` on Freebsd and `/usr/share/examples/acme.sh` on Linux.
- `acmesh_accounts_path`: The path where home directories for acme.sh are created. Defaults to `/usr/local/etc/acme` on Freebsd and `/etc/acme` on Linux.
- `acmesh_install_path`: The path where certificates are installed after they are issued or renewed. Defaults to `/usr/local/etc/ssl` on Freebsd and `/etc/ssl` on Linux.
- `acmesh_cron_path`: The path to `cron.d` on the host. Defaults to `/usr/local/etc/cron.d` on Freebsd and `/etc/cron.d` on Linux.
- `acmesh_repo`: The upstream git repository for acme.sh. Defaults to https://github.com/acmesh-official/acme.sh
- `acmesh_version`: The upstream branch or commit to fetch for installation **on Debian**. Defaults to `HEAD`.
- `dnsapi_providers`: A list of supported DNS api providers to copy the hook scripts for. Determines what providers are supported by `acme_client`.
